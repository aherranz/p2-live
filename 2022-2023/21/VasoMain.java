public class VasoMain {
  public static void main(String[] args) {
    Vaso v = new Vaso(200);
    try {
      v.llenar(250);
    }
    catch(CapacidadSuperada e) {
      System.err.println("Se ha superado la capacidad del vaso!");
    }
    System.out.println("Fin de ejecución de VasoMain");
  }
}
