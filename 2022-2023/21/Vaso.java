/**
 * Las instancias de {@code Vaso} representan vasos de la
 * realidad, que se pueden llenar y vaciar.
 *
 * @inv. {@code
 *   this.contenido() <= this.capacidad()
 *   && this.contenido() >= 0
 *   && this.capacidad() > 0
 * }
 */
public class Vaso {
  private int capacidad_ml;
  private int contenido_ml;

  /**
   * Crea un vaso con capacidad máxima {@code capacidad} en
   * mililitros
   *
   * @pre. {@code capacidad > 0}
   * @post. se ha creado el vaso
   */
  public Vaso(int capacidad) {
    contenido_ml = 0;
    capacidad_ml = capacidad;
  }

  /**
   * Añade una cantidad al vaso
   *
   * @pre. {@code cantidad_ml + contenido() <= capacidad()}
   * @post. el contenido del vaso se habrá incrementado en
   * {@code cantidad_ml}
   * @param cantidad_ml Mililitros a añadir al vaso
   */
  void llenar(int cantidad_ml) {
    // Programación "defensiva"
    if (cantidad_ml + contenido() > capacidad()) {
      throw new CapacidadSuperada();
    }
    contenido_ml = contenido_ml + cantidad_ml;
  }

  /**
   * Vacía una cantidad al vaso
   *
   * @pre. {@code cantidad_ml <= contenido()}
   * @post. el contenido del vaso se habrá reducido en
   * {@code cantidad_ml}
   * @param cantidad_ml Mililitros a vaciar del vaso
   */
  void vaciar(int cantidad_ml) {
    contenido_ml = contenido_ml - cantidad_ml;
  }

  /**
   * Dice qué capacidad tiene el vaso en mililitros
   *
   * @return capacidad del vaso
   */
  public int capacidad() {
    return capacidad_ml;
  }

  /**
   * Dice qué contenido tiene el vaso en mililitros
   *
   * @return contenido del vaso
   */
  public int contenido() {
    return contenido_ml;
  }
}
