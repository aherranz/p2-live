public class Perro extends Mamifero {
  public Perro() {
  }

  public void ladrar() {
    System.out.println("Guau!");
  }

  public void comunicar() {
    ladrar();
  }
}
