public class Sesion19 {
  public static void main(String[] args) {
    System.out.println("Operaciones sobre m (Mamifero)");
    Mamifero m = new Mamifero();
    m.comunicar();

    assert m instanceof Mamifero;
    assert m instanceof Object;

    System.out.println("Operaciones sobre p (Perro)");
    Perro p = new Perro();
    p.comunicar();

    assert p instanceof Perro;
    assert p instanceof Mamifero;
    assert p instanceof Object;

    p.ladrar();

    System.out.println("Ejecutamos m = p");
    m = p;
    System.out.println("Operaciones sobre m");
    m.comunicar();
    // m.ladrar();

    m = g;

    try {
      (Perro)m.ladrar();
    }
    catch(Exception e) {
      // Resolver el problema
    }

    System.out.println("Ejecutamos p2 = m");
    Perro p2 = (Perro)m;
    System.out.println("Operaciones sobre p2");
    p2.comunicar();
    p2.ladrar();










    System.out.println("Fin tests");
  }
}
