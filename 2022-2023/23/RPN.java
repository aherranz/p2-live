import tads.IStack;
import tads.LinkedStack;
import java.util.Scanner;

/**
 * Programa que evalua una expresión en notación
 * polaca inversa.
 *
 * SE ASUME (PRECONDICIÓN) QUE NO HAY ERRORES DE
 * SINTAXIS EN LA EXPRESIÓN (no hay caracteres
 * raros, etc.)
 */
public class RPN {
  public static void main(String[] args) {
    IStack<Integer> pila = new LinkedStack<Integer>();
    Scanner entrada = new Scanner(System.in);
    while (entrada.hasNext()) {
      String token = entrada.next();
      int operando, operando1, operando2;
      // CUIDADO: el siguiente código es horrible con tanta repetición
      // del mismo esquema, hay que cambiarlo y "refactorizarlo" para
      // que no haya tanto copy&paste
      switch (token) {
      case "+":
        operando2 = pila.peek();
        pila.pop();
        operando1 = pila.peek();
        pila.pop();
        pila.push(operando1 + operando2);
        break;

      case "-":
        operando2 = pila.peek();
        pila.pop();
        operando1 = pila.peek();
        pila.pop();
        pila.push(operando1 - operando2);
        break;

      case "*":
        operando2 = pila.peek();
        pila.pop();
        operando1 = pila.peek();
        pila.pop();
        pila.push(operando1 * operando2);
        break;

      case "/":
        operando2 = pila.peek();
        pila.pop();
        operando1 = pila.peek();
        pila.pop();
        pila.push(operando1 / operando2);
        break;

      case "^":
        operando2 = pila.peek();
        pila.pop();
        operando1 = pila.peek();
        pila.pop();
        pila.push((int)Math.pow(operando1, operando2));
        break;

      case "=":
        operando = pila.peek();
        pila.pop();
        System.out.println(operando);
        break;

      default:
        // ¡Es un operando!
        operando = Integer.parseInt(token);
        pila.push(operando);
        break;
      }
    }
  }
}
