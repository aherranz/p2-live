public class IListTest {
  private static final int N = 5;

  public static void main(String[] args) {
    System.out.println("Probando ArrayList");
    test1(new ArrayList<String>());
    test2(new ArrayList<String>());
    test3(new ArrayList<String>());
    test4(new ArrayList<String>());
  }

  private static void test1(IList<String> l) {
    System.err.println(l);

    assert l.size() == 0;

    assert l.indexOf("Hola") == -1;

    for (int i = 0; i < N; i++) {
      l.add(l.size(), "D" + i);
    }

    System.err.println(l);

    assert "[D0, D1, D2, D3, D4]"
      .equals(l.toString());

    assert "D3".equals(l.get(3));

    assert l.indexOf("D2") == 2;

    System.err.println(l.indexOf("XXX"));

    assert l.indexOf("XXX") == -1;

    assert l.size() == N;

    assert l.indexOf("NO ESTA EN LA LISTA") == -1;

    l.remove(2);

    l.remove("D3");

    l.add(l.size(), "D4");

    System.err.println(l);

    assert l.indexOf("D4") == 2;

    l.remove("D4");

    assert l.size() == 3 : "Lista: " + l;
  }

  private static void test2(IList<String> l) {
    assert l.size() == 0;

    for (int i = 0; i < N; i++) {
      l.add(0, "X"+i);
    }

    System.err.println(l);

    l.set(3, "TRES");

    assert l.size() == N;

    System.err.println(l);

    assert l.get(3).equals("TRES");

    for (int i = 0; i < N; i++) {
      if (i != 3)
        assert l.get(i).equals("X"+(N-i-1));
    }

    IList<String> subl = l.subList(1,3);

    assert subl.size() == 2 : "Sublista: " + subl;

    assert l.size() == N;

    System.err.println(l);
    System.err.println(subl);

  }

  private static void test3(IList<String> l) {
    assert l.size() == 0;

    for (int i = 0; i < N; i++) {
      l.add(l.size(), "M" + i);
    }

    System.err.println(l);

    l.add(N / 2, "MEDIO");

    System.err.println(l);

    assert l.size() == N + 1;

    for (int i = 0; i < N / 2; i++) {
      assert l.get(i).equals("M" + i);
    }

    assert l.get(N / 2).equals("MEDIO");

    for (int i = 1 + N / 2; i < N + 1; i++) {
      assert l.get(i).equals("M" + (i - 1));
    }
  }

  private static void test4(IList<String> l) {
    assert l.size() == 0;

    for (int i = 0; i < N; i++) {
      l.add("M"+i);
    }

    System.err.println(l);

    l.add(N/2, "MEDIO");

    System.err.println(l);

    assert l.size() == N + 1;

    for (int i = 0; i < N/2; i++) {
      assert l.get(i).equals("M"+i);
    }

    assert l.get(N/2).equals("MEDIO");

    for (int i = 1 + N/2 ; i < N + 1; i++) {
      assert l.get(i).equals("M"+(i-1));
    }
  }
}
