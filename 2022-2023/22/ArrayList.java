public class ArrayList<T> implements IList<T> {
  private Object[] data;

  public ArrayList() {
    data = new Object[0];
  }

  // Método privado para redimensionar un array en n
  // (n puede ser negativo)
  private void redimensionar(int n) {
    Object[] aux = new Object[data.length + n];
    for (int j = 0; j <= ultimo; j++) {
      aux[j] = data[j];
    }
    data = aux;
  }

  public void add(int index, T elem) {
    redimensionar(1);
    for (int i = data.length - 1; i >= index; i--) {
      data[i+1] = data[i];
    }
    data[index] = elem;
  }

  public void add(T elem) {
    add(data.length, elem);
  }

  public T get(int index) {
    return (T)data[index];
  }

  public int size() {
    return data.length;
  }

  public void set(int index, T elem) {
  }

  public int indexOf(T elem) {
    // OPCION 1:
    for(int i = 0; i < data.length; i++) {
      if (elem.equals(data[i])) {
        return i;
      }
    }
    return -1;

    // // OPCION 2:
    // int i = data.length - 1;
    // while (i >= 0 && !elem.equals(data[i])){
    //   i--;
    // }
    // return i;
  }

  public void remove(int index) {
  }

  public void remove(T elem) {
    remove(indexof(elem));
  }

  public IList<T> subList(int start, int end) {
    ArrayList<T> sublista = new ArrayList<String>();

    ....
    return sublista;
  }

  public String toString() {
    String result = "";
    for (int i = 0; i < data.length; i++) {
      result = result + "," + data[i];
    }
    return result;
  }
}
