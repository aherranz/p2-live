public interface IList<T> {
  void add(int index, T elem);
  void add(T elem);
  T get(int index);
  int size();
  void set(int index, T elem);
  int indexOf(T elem);
  void remove(int index);
  void remove(T elem);
  IList<T> subList(int start, int end);
}
