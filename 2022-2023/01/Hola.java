
public class Hola {
  public static void main(String[] args) {
    int n = args.length;
    if (n == 0) {
      System.out.println("Hola mundo");
    }
    else {
      System.out.print("Hola");
      for (int i = 0; i < n; i++) {
        System.out.print(" " + args[i]);
      }
      System.out.println();
    }
  }
}
