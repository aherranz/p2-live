public class PruebaStatic {
  static void intercambiar(int x, int y) {
    int tmp = x;
    x = y;
    y = tmp;
  }

  static boolean esPar(int x) {
    return x % 2 == 0;
  }

  public static void main(String[] args) {
    int a, b;
    a = 27;
    b = 42;
    intercambiar(a,b);
    System.out.println(a + " - " + b);
    System.out.println(esPar(21));

    System.out.println(Playlist.max_canciones);
    System.out.println(Playlist.max_duracion);
    Playlist l1 = new Playlist();
    Playlist l2 = new Playlist();
    System.out.println(l1.max_canciones);
    System.out.println(l1.max_duracion);
    Playlist.max_canciones = 21;
    System.out.println(l2.max_canciones);
    System.out.println(l2.max_duracion);



  }
}
