public class Sesion07 {
  public static void main(String[] args) {
    ////////////////////////////////////////////
    // Versión más "conceptual"
    boolean[][] m = new boolean[4][];
    System.out.println(m);
    System.out.println(m[0]);

    for (int i = 0; i < m.length; i++) {
      // Cada array podría tener long variable
      m[i] = new boolean[3];
      for (int j = 0; j < m[i].length; j++) {
        m[i][j] = false;
      }
    }
    System.out.println(m[0]);
    System.out.println(m[0][0]);
    System.out.println(m.length);
    System.out.println(m[0].length);

    ////////////////////////////////////////////
    // Versión más "práctica"
    m = new boolean[4][3];

    System.out.println(m.length);
    System.out.println(m[0].length);


    Palo[][] ps = new Palo[4][3];
    Entero[][] es = new Entero[4][3];

    System.out.println(ps[1][2]);
    System.out.println(es[1][2]);

    // Sobre arrays de objetos

    Palo.DIAMANTE
     Palo.CORAZON

  }
}
