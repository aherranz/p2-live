public class Entero {
  private int i;
  private Entero() {
  }

  public Entero(int x) {
    i = x;
  }

  public int dato() {
    return i;
  }
}
