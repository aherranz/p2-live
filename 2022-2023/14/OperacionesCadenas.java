class NodoStr {
   String dato;
   NodoStr siguiente;
}

public class OperacionesCadenas {
   public static NodoStr insertarI(NodoStr l, String s) {
      NodoStr nuevo = new NodoStr();
      nuevo.dato = s;
      nuevo.siguiente = l;
      return nuevo;
   }

   public static int longitud(NodoStr l) {
      NodoStr aux = l;
      int n = 0;
      while (aux != null) {
         n++;
         aux = aux.siguiente;
      }
      return n;
   }

   /**
    * Inserta s al final de la cadena l devolviendo la cadena desde el
    * primer elemento.
    */
   public static NodoStr insertarF(NodoStr l, String s) {
      NodoStr nuevo = new NodoStr();
      nuevo.dato = s;
      nuevo.siguiente = l;
      return nuevo;
   }

   public static void main(String[] args) {
      NodoStr l = new NodoStr();
      l.dato = "Hola";
      l.siguiente = new NodoStr();
      l.siguiente.dato = "Adios";

      l = insertarI(l, "Primero");

      int n = longitud(l);
      l = null;

      l = insertarF(l, "Final");

      n = longitud(l);

   }
}
