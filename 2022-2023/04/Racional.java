public class Racional {
  private int n;
  private int d;

  public Racional(int num, int den) {
    n = num;
    d = den;
  }

  public Racional(int i) {
    n = i;
    d = 1;
  }

  private static divisor(int x, int y) {
    // calcular un divisor de x e y
    for (int i = 2; i <= x; i++) {
      if (x % i == 0 && y % i == 0) {
        return i;
      }
    }
    return 1;
  }

  private void simpl() {
    int divisor = divisor(n,d);
    while (divisor > 1) {
      n = n / divisor;
      d = d / divisor;
      divisor = divisor(n,m);
    }
  }

  public String toString() {
    if (d == 1)
      return "" + n;
    else
      return n + " / " + d;
  }
}
