public class Naipe {
  private String palo = "CORAZONES";
  private String valor = "AS";

  public String getPalo() {
    return palo;
  }
  public String getValor() {
    return valor;
  }
  public void setPalo(String palo) {
    this.palo = palo;
  }
  public void setValor(String valor) {
    this.valor = valor;
  }
}
