public class Sesion06 {
  public static void main(String[] args) {
    Naipe n1 = new Naipe("TREBOLES", "AS");
    Naipe n2 = new Naipe("DIAMANTES", "2");
    System.out.println(n1);
    System.out.println(n2);
    // Para ejecutar los asserts hace falta el flag -ea: java -ea Sesion06
    assert !n1.equals(n2) : "n1 y n2 deberían ser no iguales";
    Naipe n3 = new Naipe("TREBOLES", "AS");
    assert n3.equals(n1) : "n3 y n1 deberían ser iguales";
  }
}
