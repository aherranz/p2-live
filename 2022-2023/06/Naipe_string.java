public class Naipe {
  private String palo;
  private int valor;

  public Naipe(String palo, String valor) {
    this.palo = palo;

    /*
    if ("AS".equals(valor))
      this.valor = 14;
    else if ("J".equals(valor))
      this.valor = 11;
    else if ("Q".equals(valor))
      this.valor = 12;
    else if ("K".equals(valor))
      this.valor = 13;
    else
      this.valor = Integer.parseInt(valor);
    */

    switch (valor) {
    case "AS":
      this.valor = 14;
      break;
    case "J":
      this.valor = 11;
      break;
    case "Q":
      this.valor = 12;
      break;
    case "K":
      this.valor = 13;
      break;
    default:
      this.valor = Integer.parseInt(valor);
    }
  }

  public String palo() {
    return palo;
  }

  public String valor() {
    return "" + valor;
  }

  public String toString() {
    return valor + " " + palo;
  }

  public boolean equals(Naipe n) {
    return palo == n.palo && valor == n.valor;
  }
}
