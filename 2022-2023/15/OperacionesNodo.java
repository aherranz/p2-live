class NodoStr {
  String dato;
  NodoStr siguiente;
}

public class OperacionesNodo {
  static NodoStr crearVacia() {
    return null;
  }

  static boolean esVacia(NodoStr cadena) {
    return cadena == null;
  }

  static NodoStr insertarP(NodoStr cadena, String s) {
    NodoStr nuevo = new NodoStr();
    nuevo.dato = s;
    nuevo.siguiente = cadena;
    return nuevo;
  }

  static int longitud(NodoStr cadena) {
    NodoStr aux = cadena;
    int contador = 0;
    while (aux != null) {
      aux = aux.siguiente;
      contador++;
    }
    return contador;
  }

  static String ultimo(NodoStr cadena) {
    NodoStr aux = cadena;
    while (aux.siguiente != null) {
      aux = aux.siguiente;
    }
    return aux.dato;
  }

  static NodoStr insertarU(NodoStr cadena, String s) {
    NodoStr ultimo = new NodoStr();
    ultimo.dato = s;
    ultimo.siguiente = null;
    if (cadena == null)
      return ultimo;
    else {
      NodoStr aux = cadena;
      while (aux.siguiente != null)
        aux = aux.siguiente;
      aux.siguiente = ultimo;
      return cadena;
    }
  }

  public static void main(String args[]) {
    NodoStr l;

    l = crearVacia();

    assert esVacia(l) : "La lista vacía debería ser vacía";

    assert longitud(l) == 0 : "La long de la lista vacía debería ser 0";

    l = insertarP(l, "Hola");

    assert !esVacia(l) : "La lista con un elemento no es vacía";

    assert longitud(l) == 1 : "La long de una lista con un elemento debería ser 1";

    for (int i = 0; i < 1000; i++) {
      l = insertarP(l, "" + i);
    }

    assert longitud(l) == 1001 : "La long de la lista tras insertar 1001 elementos es 1001";

    assert ultimo(l).equals("Hola") : "El último elemento de esta lista debería ser \"Hola\" pero es otra cosa";

    /* l === ["999", "998"..., "1", "0", "Hola"] */

    l = insertarU(l, "Último");

    /* l === ["999", "998"..., "1", "0", "Hola", "Último"] */

    assert longitud(l) == 1002;
    assert ultimo(l).equals("Último");

    l = crearVacia();
    l = insertarU(l, "Último");

    assert longitud(l) == 1;
    assert ultimo(l).equals("Último");

    System.out.println("OperacionesNodo.main terminado");
  }
}
