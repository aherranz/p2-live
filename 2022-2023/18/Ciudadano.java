public class Ciudadano {
  private String nombre;
  public Ciudadano(String nombre) {
    this.nombre = nombre;
  }
  public boolean equals(Object o) {
    // !(null instanceof Ciudadano)
    if (!(o instanceof Ciudadano)) {
      return false;
    }
    // o != null && o instanceof Ciudadano
    Ciudadano c = (Ciudadano)o;
    return c.nombre.equals(nombre);
  }

  public static void main(String[] args) {
    Ciudadano bruce = new Ciudadano("Bruce Wayne");
    Object batman = new Ciudadano("Bruce Wayne");
    assert bruce.equals(batman)
      : "bruce y batman deberían ser iguales";
    assert !bruce.equals(null);
    assert !bruce.equals("Hola");
    assert !bruce.equals(42);
    assert !bruce.equals(new Ciudadano("Clark Kent"));
    System.out.println("Tests finalizados");
  }
}
