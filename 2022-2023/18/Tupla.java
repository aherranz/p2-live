public class Tupla<T1, T2> {
  private T1 x;
  private T2 y;

  public Tupla(T1 fst,
               T2 snd) {
    x = fst;
    y = snd;
  }

  public T1 fst() {
    return x;
  }

  public T2 snd() {
    return y;
  }

  public String toString() {
    return "(" + fst() + ", " + snd() + ")";
  }

  //////////////////////////////////////////////////////////////////////
  // Tests para la clase Tupla
  public static void main(String[] args) {
    Tupla<Integer,Integer> t1 =
      new Tupla<Integer, Integer>(5,1);
    Tupla<Boolean,Boolean> t2 =
      new Tupla<Boolean, Boolean>(true, false);
    Tupla<String,String> t3 =
      new Tupla<String, String>("Ángel","Herranz");
    Tupla<String,Boolean> t4 =
      new Tupla<String, Boolean>("Ángel",true);

    assert t1.snd().equals(1);
    assert t2.fst();
    assert t3.snd().equals("Herranz");
    assert t4.snd();

    // Sintaxis para crear tuplas sin necesidad de establecer su tipo
    // en el new, intentando que Java infiera el tipo adecuado de los
    // parámetros de tipos (T1 y T2)
    Tupla<String,String> t5 = new Tupla<>(null,"Herranz");
    System.out.println(t5);
    System.out.println(new Tupla<>(null,"Herranz"));
    System.out.println(new Tupla<>(null,"Herranz").getClass());
    System.out.println(new Tupla<>(null,"Herranz").fst());
    String s = t5.fst();
    // Para probar de qué tipo es primer elemento de la tupla new
    // Tupla<>(null,"Herranz") sin más contexto (intenta cambiar el
    // tipo de o a String
    Object o = new Tupla<>(null,"Herranz").fst();

    // Es posible crear tuplas de tuplas :o
    Tupla<Tupla<String, Integer>, Ciudadano> t6 =
      new Tupla<>(new Tupla<>("Hola", 42),
                  new Ciudadano("Clark Kent"));

    System.out.println("Fin de tests");
  }
}
