import java.util.Scanner;

public class ImprimirPlaylist {
  public static void main(String[] args) {
    Scanner entrada;
    String linea;
    Cancion[] playlist;

    entrada = new Scanner(System.in);

    linea = entrada.nextLine();

    int n = Integer.parseInt(linea);
    playlist = new Cancion[n];

    for (int j = 0; j < n; j++) {
      String t = entrada.nextLine();
      String i = entrada.nextLine();
      String vs = entrada.nextLine();
      int v = Integer.parseInt(vs);
      playlist[j] = new Cancion(t,i,v);
    }
    /* Imprimimos en orden inverso! */
    for (int j = n-1; j >= 0; j--) {
      System.out.println(playlist[j]);
    }
  }
}
