public class Cancion {
  private String titulo;
  private String interprete;
  private int valoracion;

  public Cancion(String t, String i, int v) {
    titulo = t;
    interprete = i;
    valoracion = v;
  }

  public String toString() {
    return String.format("|%s|%s|%d|",
                         titulo, interprete, valoracion);
  }
}
