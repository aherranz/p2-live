
public class Sesion08 {
  public static void main(String[] args) {
    java.util.Scanner entrada = new java.util.Scanner(System.in);
    String linea;
    linea = entrada.nextLine();
    System.out.println("Primera línea: '" + linea + "'");
    System.out.println("Longitud: " + linea.length());
    linea = entrada.nextLine();
    System.out.println("Segunda línea: '" + linea + "'");
    System.out.println("Longitud: " + linea.length());
  }
}
