class Punto2D {
  private double r;
  private double a;

  public void cambiarX(double x) {
    // Para casa;
  }

  public void cambiarY(double y) {
    // Para casa;
  }

  public double x() {
    return r * Math.cos(a);
  }

  public double y() {
    return r * Math.sin(a);
  }

  public void rotar(double rad) {
    a = a + rad;
  }

  public String toString() {
    return String.format("(%f, %f)", x(), y());
  }
}
