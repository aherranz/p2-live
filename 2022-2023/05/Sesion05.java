public class Sesion05 {
  public static void main(String[] args) {
    Punto2D p = new Punto2D();
    p.cambiarX(0.0);
    p.cambiarY(1.0);
    System.out.println(p);
    p.rotar(Math.PI / 4);
    System.out.format("(%f, %f)\n", p.x(), p.y());
  }
}
