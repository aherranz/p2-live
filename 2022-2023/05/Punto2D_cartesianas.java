class Punto2D {
  private double x;
  private double y;

  void cambiarX(double x) {
    this.x = x;
  }

  void cambiarY(double y) {
    this.y = y;
  }

  double x() {
    return x;
  }

  double y() {
    return y;
  }

  void rotar(double rad) {
    double r = Math.sqrt(x*x + y*y);
    double a = Math.atan(y / x);
    a = a + rad;
    x = r * Math.cos(a);
    y = r * Math.sin(a);
  }

  public String toString() {
    return String.format("(%f, %f)", x, y;
  }
}
