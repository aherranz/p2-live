import java.util.Scanner;
import java.io.File;

public class ProblemaException {

  static void f() {
    try {
      Scanner s = new Scanner(new File("f.txt"));
    }
    catch (Exception e) {
      System.err.println("No se ha podido abrir f.txt");
      System.exit(1);
    }
  }

  static void g() throws Exception {
    Scanner s = new Scanner(new File("g.txt"));
  }

  public static void main(String[] args) {
    f();
    try {
      g();
    }
    catch (Exception exc) {
      System.err.println("Error en llamada a g()");
      System.exit(1);
    }

  }
}
