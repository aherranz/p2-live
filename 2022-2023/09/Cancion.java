public class Cancion {
  private String titulo;
  private String interprete;
  private int valoracion;

  public Cancion(String t, String i, int v) {
    titulo = t;
    interprete = i;
    valoracion = v;
  }

  public String titulo() {
    return titulo;
  }

  public String toString() {
    return String.format("| %-20s | %-20s | %2d |",
                         titulo, interprete, valoracion);
  }
}
