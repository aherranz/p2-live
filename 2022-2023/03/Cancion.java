public class Cancion {
  String titulo;
  String interprete;
  int duracionSeg;
  boolean explicit;
  String album;
  int[] publicacion = new int[3];

  Cancion(String t,
          String i,
          int dur,
          boolean e,
          String album,
          int d,
          int m,
          int a) {
    titulo = t;
    interprete = i;
    duracionSeg = dur;
    explicit = e;
    this.album = album;
    publicacion[0] = d;
    publicacion[1] = m;
    publicacion[2] = a;
  }

  void setExplicit(boolean e) {
    explicit = e;
  }

  String minutos() {
    return duracionSeg / 60 + "mins";
  }

  public String toString() {
    return
       "| " + titulo +
      " | " + interprete +
      " | " + minutos() +
      " | " + publicacion[0] + "/" +
      publicacion[1] + "/" +
      publicacion[2] +
      " |";
  }
}
