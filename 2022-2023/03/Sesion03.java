public class Sesion03 {
  public static void main(String[] args) {
    Cancion[] canciones = new Cancion[5];

    canciones[0] = new Cancion("Wish you were here",
                               "Pink Floyd",
                               339,
                               false,
                               "Wish you were here",
                               21,
                               9,
                               1974);

    canciones[1] = new Cancion("Wish you were here",
                               "Pink Floyd",
                               339,
                               false,
                               "Wish you were here",
                               21,9, 1974);

    for (int i = 0; i < canciones.length; i++) {
      if (canciones[i] != null) {
        System.err.println("EMPIEZA LA CANCION " + i);
        System.err.println("EL ELEMENTO ES " + canciones[i]);
        System.out.println(canciones[i]);
        System.err.println("TERMINA LA CANCION " + i);
      }
    }
  }
}
