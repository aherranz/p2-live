import geometria.*;

public class TestGeometria {
  public static void main(String[] args) {
    Punto2D p = new Punto2D(2.0, 2.0);
    Circulo c = new Circulo(p, 1.0);
    Cuadrado q = new Cuadrado(new Punto2D(), 2.0);

    assert p.x() == 2.0 && p.y() == 2.0;
    assert c.area() == Math.PI;
    assert q.area() == 4.0;

    System.out.println("TESTS pasados (comprueba -ea)");
  }
}
