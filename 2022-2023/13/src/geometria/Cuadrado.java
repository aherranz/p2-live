package geometria;

public class Cuadrado {
  private Punto2D c;
  private double l;

  public Cuadrado(Punto2D c, double l) {
    this.c = c;
    this.l = l;
  }

  public double area() {
    return l * l;
  }
}
