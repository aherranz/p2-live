package geometria;

public class Punto2D {
  private double x;
  private double y;

  public Punto2D() {
    x = 0.0;
    y = 0.0;
  }

  public Punto2D(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public double x() {
    return x;
  }

  public double y() {
    return y;
  }
}
