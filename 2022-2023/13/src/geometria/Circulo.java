package geometria;

public class Circulo {
  private Punto2D c;
  private double r;

  public Circulo(Punto2D p, double r) {
    c = p;
    this.r = r;
  }

  public double area() {
    return Math.PI * r * r;
  }
}
