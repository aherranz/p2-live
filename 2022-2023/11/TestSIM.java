public class TestSIM {
  public static void main(String[] args) {
    SIM lucia = new SIM("Lucía");
    assert "Lucía".equals(lucia.nombre()) :
      "el nombre de lucia debería ser Lucía pero es: "+ lucia.nombre();
    assert lucia.quease() == SIM.Actividad.DORMIR :
      "lucia debería estar durmiendo pero está: " + lucia.quease();
    lucia.simular(1);
    assert lucia.quease() == SIM.Actividad.DORMIR;
    assert "Lucía".equals(lucia.nombre());
    lucia.simular(8);
    assert lucia.quease() == SIM.Actividad.COMER;
  }
}
