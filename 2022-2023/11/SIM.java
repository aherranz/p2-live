/**
 * Las instancias de SIM representan seres humanos en una
 * simulación extremadamente simplista.
 *
 * @author Ángel Herranz
 */
public class SIM {
  public static enum Actividad { DORMIR, COMER, SOCIALIZAR };

  private String nombre;
  /* Actividad que está haciendo ahora mismo */
  private Actividad queHace;
  /* Tiempo que lleva haciendo la actividad queHace */
  private int tiempoPasado;
  private SIM amigo;

  /**
   * TODO
   */
  public SIM(String nombre) {
    this.nombre = nombre;
    queHace = Actividad.DORMIR;
    amigo = null; /* al crearlo no tiene amigos */
    tiempoPasado = 0;
  }

  /**
   * Simula el paso del tiempo para este SIM. El SIM, con
   * el paso del tiempo, cambia de actividad y acumula la
   * información de la actividad anterior.
   *
   * @param tiempoHoras tiempo simulado (en horas)
   */
  public void simular(int tiempoHoras) {
    tiempoPasado = tiempoPasado + tiempoHoras;

    if (queHace == Actividad.DORMIR &&
        tiempoPasado > 7) {
      queHace = Actividad.COMER;
      tiempoPasado = 0;
    }
    /* TODO: simular el resto de situaciones */

  }

  /**
   * TODO
   */
  public void hacerAmigo(SIM otro) {
    this.amigo = otro;
  }

  /**
   * TODO
   */
  public String nombre() {
    return this.nombre;
  }

  /**
   * TODO
   */
  public Actividad quease() {
    return queHace;
  }

  /**
   * Informa sobre el tiempo dedicado a la actividad indicada.
   *
   * @param a actividad sobre la que se quiere conocer el tiempo
   * dedicado
   * @return número de horas dedicadas a la actividad indicada
   */
  public int estadistica(Actividad a) {
    return 0;
  }

  /**
   * TODO
   */
  public SIM amigo() {
    return amigo;
  }
}
