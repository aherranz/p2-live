public class LinkedListStr implements IListStr {
  private NodeStr first;

  public LinkedList() {
    first = null;
  }

  public void add(String elem) {
  }

  public void add(int index, String elem) {
  }

  public String get(int index) {
    return null;
  }

  public int size() {
    return -1;
  }

  public void set(int index, String elem) {
  }

  public int indexOf(String elem) {
    return -1;
  }

  public void remove(int index) {
  }

  public void remove(String elem) {
  }

  public IListStr subList(int start, int end) {
    return null;
  }

  public String toString() {
    if (first == null) {
      return "[]";
    }
    else {
      String resultado = "[";
      resultado = resultado + first.elem;
      NodeStr aux = first.next;
      while(aux != null) {
        resultado = ", " + aux.elem;
        aux = aux.next;
      }
      resultado = resultado + "]";
      return resultado;
    }
  }

  public boolean equals(Object o) {
    if (o == null)
      return false;

    boolean result = true;
    if (o instanceof IListStr) {
      IListStr l = (IListStr)o;
      if (size() != l.size())
        return false;
      // TODO: comparar elemento a elemento
    }
    else {
      return false;
    }

  }
}
