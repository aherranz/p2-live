public class ListStrTest {
  private static IListStr crearListaVacia() {
    return new LinkedListStr();
  }

  public static void main(String[] args) {
    int N = 100;

    IListStr l = crearListaVacia();
    System.err.println(l);
    assert l.size() == 0;
    l.add(0, "Hola");
    System.err.println(l);
    assert l.size() == 1;
    assert "Hola".equals(l.get(0));

    l = crearListaVacia();
    for (int i = 0; i < N; i++) {
      String dato = Integer.toString(i);
      l.add(dato);
      System.out.println(l);
      assert l.get(0).equals("0");
      assert l.get(l.size() - 1).equals(dato);
      assert l.size() == i + 1;
    }
    assert l.size() == N;
    assert l.get(0).equals("0");
    assert l.get(N-1).equals(Integer.toString(N-1));

    l.add(N/2, "Medio");
    assert l.size() == N+1;
    assert l.get(0).equals("0");
    assert l.get(N/2).equals("Medio");
    assert l.get(N).equals(Integer.toString(N-1));

    IListStr l1 = new LinkedList();
    IListStr l2 = new LinkedList();
    assert l1.equals(l2);

    l = crearListaVacia();
    for (int i = 0; i < N; i++) {
      String dato = Integer.toString(i);
      l.add(dato);
    }
    l.remove(1);
    assert l.get(0).equals("0");
    assert l.get(1).equals("2");
    assert l.get(2).equals("3");
    assert l.size() == N - 1;
  }

  l1 = crearListaVacia();
  l2 = crearListaVacia();
  l1.add("A");

  // Aserciones sobre equals:
  assert !l1.equals(null);
  assert !l1.equals(new Integer(42));
  assert !l1.equals("A");
  assert l1.equals(l1);
  assert !l1.equals(l2);
  assert !l2.equals(l1);

  // Aserciones sobre toString!
  assert !"".equals(crearListaVacia().toString());
  assert "[]".equals(crearListaVacia().toString());
  l1 = crearListaVacia();
  l1.add("A");
  l1.add("B");
  l1.add("C");
  assert "[A ,B ,C]".equals(l1.toString());

}
