public interface IListStr {
  void add(String elem);

  void add(int index, String elem);

  String get(int index);

  int size();

  void set(int index, String elem);

  int indexOf(String elem);

  /**
   * Quita de la lista el elemento en la posicion {@code index}.
   *
   * @pre. {@code index} mayor o igual que 0 y menor que {@code size()}
   * @post. {@code this} después de ejecutar representa una lista como
   *        {@code this} antes de ejecutar eliminando el elemento que
   *        ocupaba la posición indicada en el parámetro {@code index}
   * @param index el índice del elemento que se va a eliminar
   * @throws RuntimeException si no se cumple la precondición
   */
  void remove(int index);

  void remove(String elem);

  IListStr subList(int start, int end);
}
