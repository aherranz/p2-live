class NodeStr {
  String elem;
  NodeStr next;

  NodeStr(String elem, NodeStr next) {
    this.elem = elem;
    this.next = next;
  }

  NodeStr(String elem) {
    this(elem, null);
  }
}
