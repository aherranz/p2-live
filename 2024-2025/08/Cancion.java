public class Cancion {
  private String t;
  private String i;
  private int v;

  public Cancion(String titulo, String interprete, int valoracion) {
    t = titulo;
    i = interprete;
    v = valoracion;
  }

  public String titulo() {
    return t;
  }
  public String interprete() {
    return i;
  }
  public int valoracion() {
    return v;
  }

  public String toString() {
    return String.format("%s:%s:%d",t,i,v);
  }

  public boolean equals(Object o) {
    if (o == null || !(o instanceof Cancion))
      return false;
    Cancion c = (Cancion)o;
    // Consideramos que dos canciones son iguales si tienen el mismo título
    return c.titulo().equals(t);
  }
}
