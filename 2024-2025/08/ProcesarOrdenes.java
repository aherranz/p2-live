import java.util.Scanner;

public class ProcesarOrdenes {
  public static void main(String[] args) {
    // Variables para representar una playlist
    Cancion[] playlist = new Cancion[4];
    int siguiente = 0;

    Scanner entrada = new Scanner(System.in);

    // Cuidado no salirse de la playlist!
    while (entrada.hasNextLine()) {
      String orden = entrada.nextLine();
      String t, a, v;      
      switch (orden) {
      case "a":
        System.err.println("He encontrado una orden de AÑADIR");
        t = entrada.nextLine();
        a = entrada.nextLine();
        v = entrada.nextLine();
        // Añadir la canción t, a, v a la lista playlist
        Cancion c = new Cancion(t, a, Integer.parseInt(v));
        playlist[siguiente] = c;
        siguiente++;
        break;
      case "r":
        System.err.println("He encontrado una orden de BORRAR (SIN IMPLEMENTAR)");
        t = entrada.nextLine();
        System.err.println(t);
        // TODO: Borrar la canción t de la lista playlist
        int i = 0;
        // Buscar la canción
        while (i < siguiente && !t.equals(playlist[i].titulo())) {
          i++;
        }
        // SE CUMPLE: i == siguiente || t.equals(playlist[i].titulo())
        if (i < siguiente) {
          // SE CUMPLE: t.equals(playlist[i].titulo())
          while (i < siguiente - 1) {
            playlist[i] = playlist[i+1];
            i++;
          }
          playlist[siguiente-1] = null;
          siguiente--;
        }
        break;
      default:
        System.err.println("ERROR EN ORDEN");
        break;
      }
    }
    for (int i = 0; i < siguiente; i++) {
      System.out.println(playlist[i]);
    }
  }
}
