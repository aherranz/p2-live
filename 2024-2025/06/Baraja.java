import java.util.Random;

public class Baraja {
  private Random generador = new Random();
  private Naipe[] cartas = new Naipe[52];
  private int restantes = 52;
  
  public Baraja() {
    int k = 0;
    for (int i = 0; i < Valor.values().length; i++) {
      for (int j = 0; j < Palo.values().length; j++) {
        cartas[k] = new Naipe(Valor.values()[i], Palo.values()[j]);
        k = k + 1;
      }
    }
  }

  /**
   * Elige aleatoriamente un naipe de la baraja, lo quita de la baraja y lo devuelve.
   */
  public Naipe elegir() {
    // De momento esto no elimina ninguna carta asi que saldrán cartas repetidas
    int siguiente = generador.nextInt(restantes);
    Naipe n = cartas[siguiente];
    cartas[siguiente] = cartas[restantes-1];
    cartas[restantes-1] = null;
    restantes--;
    return n;
  }
}
