public class Naipe {
  private Palo palo;
  private Valor valor;

  public Naipe(Valor valor, Palo palo) {
    this.palo = palo;
    this.valor = valor;
  }

  public String toString() {
    String[] palosGuapo = {"♦", "♣", "♥", "♠"};
    String valorGuapo;
    switch (valor) {
    case VALET: valorGuapo = "J"; break;
    case DAMA: valorGuapo = "Q"; break;
    case REY: valorGuapo = "K"; break;
    case AS: valorGuapo = "A"; break;
    default: valorGuapo = "" + (valor.ordinal() + 2);
    }
    return valorGuapo + palosGuapo[palo.ordinal()];
  }
}
