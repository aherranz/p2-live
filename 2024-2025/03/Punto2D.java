class Punto2D {
  double x, y;

  Punto2D(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public boolean equals(Object o) {
    if (o != null && o instanceof Punto2D) {
      Punto2D p = (Punto2D)o;
      return this.x == p.x && this.y == p.y;
    }
    else {
      return false;
    }
      
  }
}
