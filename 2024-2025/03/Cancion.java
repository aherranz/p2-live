class Cancion {
  // Variables que definen los datos de una canción cualquiera
  String titulo;
  String interprete;
  int duracion;
  float valoracion;

  // Constructor: cómo podemos crear canciones
  Cancion(String t, String i, int d) {
    titulo = t;
    interprete = i;
    duracion = d;
  }

  // Observador: devuelve un string que representa la duración en minutos.
  String minutos() {
    return duracion / 60 + " mins";
  }  

  // Observador: devuelve un string que representa la canción.
  public String toString() {
    return "| " + titulo +
      " | " + interprete +
      " | " + minutos() +
      " | " + valoracion +
      " |";
  }  
}
