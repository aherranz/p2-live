import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class Grammy {
  public static void main(String[] args) throws FileNotFoundException {
    // Comprobar que el programa se invoca con un argumento
    if (args.length != 1) {
      System.err.println("USO: java Grammy FICHERO_NOMINADOS");
      System.exit(1);
    }

    // Abrir el fichero indicado en el argumento del programa
    File ficheroNominados = new File(args[0]);

    // Crear un scanner para poder leer datos del fichero
    Scanner entrada = new Scanner(ficheroNominados);

    // Leer el número de canciones en el fichero
    int nCanciones = entrada.nextInt();
    // Saltar línea (después del entero sólo hay un cambio de línea)
    entrada.nextLine();

    if (nCanciones < 1) {
      System.err.println("No hay canciones en el fichero " + args[0]);
      System.exit(1);
    }
    
    // Declarar una variable array de canciones y crear el array
    Cancion[] canciones = new Cancion[nCanciones];

    // Leer las canciones del fichero
    for (int j = 0; j < canciones.length; j++) {
      String t;
      String i;
      int d;
      float v;
      t = entrada.nextLine();
      i = entrada.nextLine();
      d = entrada.nextInt();
      entrada.nextLine();
      v = entrada.nextFloat();
      entrada.nextLine();
      // Crear la canción que guardamos en la posición j-ésima
      canciones[j] = new Cancion(t, i, d);
      canciones[j].valoracion = v;
    }

    // Comprobar que hemos leido las canciones
    for (int j = 0; j < canciones.length; j++) {
      System.out.println(canciones[j]);
    }

    // Buscar mejor canción
    Cancion mejor = canciones[0];
    for (int j = 1; j < canciones.length; j++) {
      if (mejor.valoracion < canciones[j].valoracion) {
        mejor = canciones[j];
      }
    }
    System.out.println("Y la mejor canción es...");
    System.out.println(mejor);
  }
}
