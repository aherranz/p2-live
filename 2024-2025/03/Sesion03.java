class Sesion03 {
  public static void main(String[] args) {
    Punto2D a = new Punto2D(1.0, 2.0);
    Punto2D b = new Punto2D(1.0, 2.0);

    System.out.println(a == b);
    System.out.println(a.equals(b));
  }
}
