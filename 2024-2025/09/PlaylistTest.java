public class PlaylistTest {
  public static void main(String[] args) throws Exception {
    Playlist p = new Playlist();
    Assert.assertEquals(p.nSongs(), 0);

    p.add(new Cancion("Columbia", "Quevedo", 5);
    Assert.assertEquals(p.nSongs(), 1);          
  }
}
