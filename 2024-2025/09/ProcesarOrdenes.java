import java.util.Scanner;

public class ProcesarOrdenes {
  public static void main(String[] args) {
    Playlist p = new Playlist();
    Scanner entrada = new Scanner(System.in);
    // Cuidado no salirse de la playlist!
    while (entrada.hasNextLine()) {
      String orden = entrada.nextLine();
      String t, a, v;      
      switch (orden) {
      case "a":
        t = entrada.nextLine();
        a = entrada.nextLine();
        v = entrada.nextLine();
        // Añadir la canción t, a, v a la lista playlist
        p.add(new Cancion(t, a, Integer.parseInt(v)));
        break;
      case "r":
        t = entrada.nextLine();
        // TODO: Borrar la canción t de la lista playlist
        p.remove(t);
        break;
      default:
        System.err.println("ERROR EN ORDEN");
        break;
      }
    }
    System.out.println(p);
  }
}
