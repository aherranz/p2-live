import java.util.Scanner;

public class App {
  public static void main(String[] args) {
    // Base de datos de usuarios simulada como array
    Usuario[] us = new Usuario[3];

    // POR HACER: poblar la "base de datos" con 3 usuarios ficticios
    us[0] = new Usuario();
    us[0].correo = "angel.herranz@upm.es";
    us[0].password = "1";
    us[0].nombre = "Herranz";
    us[0].edad = 55;
      
    us[1] = new Usuario();
    us[1].correo = "julio.marino@upm.es";
    us[1].password = "2";
    us[1].nombre = "Mariño";
    us[1].edad = 55;
      
    us[2] = new Usuario();
    us[2].correo = "susana.munoz@upm.es";
    us[2].password = "3";
    us[2].nombre = "Susana";
    us[2].edad = 50;
      
    // Para poder leer de la entrada estándar (teclado)
    Scanner entrada = new Scanner(System.in);
        
    // Pedir credenciales al usuario (correo y contraseña)
    System.out.print("Introduzca su correo: ");
    String email = entrada.nextLine();

    System.out.print("Introduzca su contraseña: ");
    String contra = entrada.nextLine();

    // POR HACER: verificar que el usurio existe y que la contraseña es
    // correcta, si es así dar la bienvenida y terminar, si no volver a
    // intentarlo tres veces
    for (int i = 0; i < us.length; i++) {
      if (us[i].correo.equals(email) && us[i].password.equals(contra)) {
        System.out.println("Bienvenida/o");
      }
    }

    // Cerrar el scanner
    entrada.close();
  }
}
