public class Sesion02 {
  public static void main(String[] args) {
    A v1;
    A v2;
    v1 = new A();
    v2 = v1;
    v2.x = 27;
    System.out.println(v1.x);
    System.out.println(v2.x);
    System.out.println(v1 == v2);
    System.out.println(v1.x == v2.x);

    System.out.println(new A().x);    
  }
}
