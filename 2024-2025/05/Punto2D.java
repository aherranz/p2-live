class Punto2D {
  private double r;
  private double a;
  /**
   * Crea un punto en 2D dadas sus coordenadas cartesianas.
   */
  public Punto2D(double x, double y) {
    r = Math.sqrt(x*x + y*y);
    a = Math.atan(y/x);
  }
  public double coordX() {
    return r * Math.cos(a);
  }
  public double coordY() {
    return r * Math.sin(a);
  }
}
