//rCalzas
public class Cancion {
  private String titulo;
  private String interprete;
  private int val;
  private int id;

  public Cancion(String titulo, String interprete, int valoracion, int i) {
    this.titulo = titulo;
    this.interprete = interprete;
    this.val = valoracion;
    this.id=i;
  }

  public String obtenerTitulo() {
    return titulo;
  }
  public String obtenerInterprete() {
    return interprete;
  }
  public int obtenerValoracion() {
    return val;
  }
   public int obtenerId() {
    return id;
  }


  public String toString() {
    return String.format("Título: %s | Intérprete: %s | Valoración: %d | ID: %d", titulo, interprete, val, id);
  }

  public boolean equals(Object o) {
    if (o == null || !(o instanceof Cancion))
      return false;
    Cancion c = (Cancion)o;
    // Consideramos que dos canciones son iguales si tienen el mismo id 
    return this.id==c.id;
  }
}
