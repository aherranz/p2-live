//rCalzas
import java.util.Comparator;
import java.util.Arrays;
import java.util.Scanner;
public class Playlist{
  Scanner entrada =new Scanner(System.in);
  private Cancion [] canciones= new Cancion[10];
  int ultVal=0;
  //constructor 
  public Playlist(){
  }
  //obeserver
  public int obtenerLongitudDePlaylist(){
    return canciones.length;
  }
  public int obtenerUltimoValor(){
    return ultVal;
  }
  //mostrar playlist 
  public void mostrarPlaylist(){
    for(int i=0;i<ultVal;i++){
      System.out.println(canciones[i]);
    }
  }
  //modifierLongitud
  public void CambiarLongitud(){
    System.out.println("Cual quieres que sea la longitud de la playlist? La longitud actual es de "+canciones.length);
    int n=entrada.nextInt();
    canciones=new Cancion[n];
    System.out.println("la nueva longitud de tu playlist es de "+canciones.length+" canciones");
  }
  //operaciones para modelar
  public void añadirCancion(){
    if(ultVal>=canciones.length)
      throw new IllegalArgumentException("Has intentado añadir una canción cuando la playlist es muy pequeña, ampliala para poder añadir más canciones ");
            
    else{
      String tit = entrada.nextLine();
      String inter = entrada.nextLine();
      int val = entrada.nextInt();
      entrada.nextLine();// consumir linea vacías 
      int fId= entrada.nextInt();
      entrada.nextLine();//consumir linea vacía 
      canciones[ultVal]=new Cancion(tit,inter,val,fId);
      ultVal++;
    }
  }

  //es mejor(yo creo )que el borrado sea lo más eficiente posible, ya que luego puedo ordenar con otro método 
  public void borrarCancion(int bId){
    //int id buscado
    boolean encontrado=false;
    for(int i=0;!encontrado && i<ultVal;i++) {
      //< ultVal, ya que así solo me recorro los huecos del array donde hay canciones e ignoro los null
      if(canciones[i].obtenerId()==bId){
        ultVal--;
        canciones[i]=canciones[ultVal];
        canciones[ultVal]=null;
        encontrado=true;
      }
    }
  }
  public void ordenarPorId(){
    Arrays.sort(canciones, Comparator.comparingInt(cancion->cancion.obtenerId()));
  }

  public void ordenarPorValoracion(){
    Arrays.sort(canciones, Comparator.comparingInt(cancion->cancion.obtenerValoracion()));
  }

  public boolean equals(Object o){
    if (o == null || !(o instanceof Playlist) )
      return false;
    Playlist p= (Playlist) o;
    if( this.obtenerLongitudDePlaylist()!=p.obtenerLongitudDePlaylist())
      return false;
    //si ya son los dos playlist con el mismo número de canciones...
    boolean iguales=true;
    for(int i=0;i<this.ultVal&& iguales;i++){
      if (this.canciones[i] == null || p.canciones[i] == null)
        iguales=false;
      if(!this.canciones[i].obtenerTitulo().equals(p.canciones[i].obtenerTitulo())||!this.canciones[i].obtenerInterprete().equals(p.canciones[i].obtenerInterprete())||this.canciones[i].obtenerValoracion()!=p.canciones[i].obtenerValoracion()||this.canciones[i].obtenerId()!=p.canciones[i].obtenerId())
        iguales=false;
    }
    return iguales;
  }
}
