package geometria;

public class Circulo {
  private Punto2D c;
  private double r;

  public Circulo(Punto2D centro, double radio) {
    c = centro;
    r = radio;
  }

  public Punto2D centro() {
    return c;
  }

  public double radio() {
    return r;
  }

  public double area() {
    return Math.PI * r * r;
  }
}
