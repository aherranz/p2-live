package geometria;

public class Cuadrado {
  private double l;
  // Internamente lo representamos con el punto Sur-Oeste!
  private Punto2D so;
  
  public Cuadrado(Punto2D centro, double lado) {
    l = lado;
    so = new Punto2D(centro.coordX() - lado / 2,
                     centro.coordY() - lado / 2);
  }

  public double lado() {
    return l;
  }
  
  public double diagonal() {
    return Math.sqrt(2.0*l*l);
  }
  
  public Punto2D centro() {
    return new Punto2D(so.coordX() + l / 2,
                       so.coordY() + l / 2);
  }
  
  public double area() {
    return l * l;
  }
}
