import geometria.*;

public class TestGeometria {
  public static void main(String[] args) {
    Punto2D p = new Punto2D(1.0, 2.0);
    assert p.coordX() == 1.0;
    assert p.coordY() == 2.0;

    Cuadrado s = new Cuadrado(p, 2.0);
    assert s.area() == 4.0;
    
    Circulo c = new Circulo(p, 2.0);
    assert c.area() == 4.0;
  }
}
