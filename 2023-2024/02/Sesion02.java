public class Sesion02 {
  public static void main(String[] args) {
    A y;
    y = new A();
    Object z = new A();
    System.out.println(y == z);
    // B w = new A();

    Persona p1 = new Persona();
    Persona p2 = new Persona();

    p1.nombre = "Ángel";
    p2.nombre = "Ángel";

    System.out.println(p1 == p2);
    System.out.println(p1.nombre == p2.nombre);

    p1 = p2;
  }
}
