public class TestSim {
  private static void testSimula0(Sim s) {
    Actividad ahora = s.haciendo();
    s.simular(0);
    assert s.haciendo() == ahora;
  }

  public static void main(String[] ags) {
    Sim lucia = new Sim("Lucía");
    assert lucia.haciendo() == Actividad.DORMIR;
    lucia.simular(1);
    assert lucia.haciendo() == Actividad.DORMIR;

    assert "Lucía".equals(lucia.nombre());

    lucia.simular(8);
    // Como han pasado 8 horas, 7 ha estado durmiendo y 1 comiendo así
    // que ahora está estudiando!
    assert lucia.haciendo() == Actividad.ESTUDIAR;

    Sim marga = new Sim("Margarita");
    lucia.hacerAmigo(marga);

    // ¿Cuál es el nombre de marga?
    assert "Margarita".equals(marga.nombre());

    // Marga es "la" amiga de Lucía pero...
    assert lucia.amigo().nombre().equals("Margarita");

    // Es Lucía la amiga de Marga? ¡No!
    // (esto es semántica de mi TAD)
    assert marga.amigo() != lucia;

    // ¿Qué pasa si simulamos el paso de 0 horas?
    testSimula0(lucia);
    testSimula0(marga);

    // ¿Qué debería hacer toString()?
    // TODO: añadir tests para toString de Sim
    
    // ¿Qué debería hacer equals()?
    // TODO: añadir tests para equals de Sim

    // ¿Qué debería hacer en concreto simular?
    Sim s = new Sim("Luis");
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.DORMIR;
    s.simular(1);
    assert s.haciendo() == Actividad.COMER;
    s.simular(1);
    assert s.haciendo() == Actividad.ESTUDIAR;
    s.simular(1);
    assert s.haciendo() == Actividad.ESTUDIAR;
    s.simular(1);
    assert s.haciendo() == Actividad.ESTUDIAR;
    s.simular(1);
    assert s.haciendo() == Actividad.ESTUDIAR;
    s.simular(1);
    assert s.haciendo() == Actividad.COMER;    
    s.simular(1);
    assert s.haciendo() == Actividad.JUGAR;    
    s.simular(1);
    assert s.haciendo() == Actividad.JUGAR;
    s.simular(1);
    assert s.haciendo() == Actividad.JUGAR;    
    s.simular(1);
    assert s.haciendo() == Actividad.JUGAR;
    s.simular(1);
    assert s.haciendo() == Actividad.JUGAR;    
    s.simular(1);
    assert s.haciendo() == Actividad.JUGAR;

    // ¿Cuántas horas ha estado comiendo?
    assert s.estadistica(Actividad.COMER) == 2;

    System.out.println("Todos los tests han pasado pero...");
    System.out.println("ASEGURATE DE QUE LO HAS EJECUTADO CON java -ea");
  }
}
