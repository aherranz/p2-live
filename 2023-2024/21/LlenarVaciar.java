import java.util.Scanner;

public class LlenarVaciar {

  private static void ejecutarDelta(Vaso v, int cantidad) {
    try {
      if (cantidad < 0)
        v.vaciar(-cantidad);
      else
        v.llenar(cantidad);
    }
    catch (CapacidadSuperada e) {
      System.err.println("No es posible llenar el vaso por encima de su capacidad");
    }
    catch (ContenidoInsuficiete e) {
      System.err.println("No es posible vaciar el vaso más allá de su contenido");
    }
  }
  
  public static void main(String[] args) {
    Vaso v = new Vaso(333);
    Scanner s = new Scanner(System.in);
    int delta;
    do {
      System.out.println(v);
      System.out.print("Indica la cantidad a llenar (positivo) o vaciar (negativo): ");
      delta = Integer.parseInt(s.nextLine());
      ejecutarDelta(v, delta);
    }
    while (delta != 0);
    System.out.println("Fin de ejecución del programa");
  }
}
