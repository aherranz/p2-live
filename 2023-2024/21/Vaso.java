/**
 * Las instancias de {@code Vaso} representan vasos de la realidad,
 * que se pueden llenar y vaciar y que tienen una capacidad y un
 * contenido
 *
 * @inv. {@code
    *   this.contenido() <= this.capacidad()
 *   && this.contenido() >= 0
 *   && this.capacidad() > 0
 * }
 */
public class Vaso {
  private int capacidad_ml;
  private int contenido_ml;

  /**
   * Crea un vaso con capacidad máxima {@code capacidad} en mililitros
   *
   * @pre. {@code capacidad > 0}
   * @post. se ha creado el vaso
   */
  public Vaso(int capacidad) {
    contenido_ml = 0;
    capacidad_ml = capacidad;
  }

  /**
   * Añade una cantidad al vaso
   *
   * @pre. {@code cantidad_ml + contenido() <= capacidad()}
   * @post. el contenido del vaso se habrá incrementado en {@code cantidad_ml}
   * @param cantidad_ml Mililitros a añadir al vaso
   * @throws CapacidadSuperada cuando no se cumple la PRE
   */
  void llenar(int cantidad_ml) throws CapacidadSuperada {
    if (!(cantidad_ml + contenido() <= capacidad())) {
      throw new CapacidadSuperada();
    }
    
    contenido_ml = contenido_ml + cantidad_ml;
  }

  /**
   * Vacía una cantidad al vaso
   *
   * @pre. {@code cantidad_ml <= contenido()}
   * @post. el contenido del vaso se habrá reducido en {@code cantidad_ml}
   * @param cantidad_ml Mililitros a vaciar del vaso
   */
  void vaciar(int cantidad_ml) throws ContenidoInsuficiente {
    if (cantidad_ml < contenido()) {
      throw new ContenidoInsuficiente();
    }
    contenido_ml = contenido_ml - cantidad_ml;
  }

  /**
   * Dice qué capacidad tiene el vaso en mililitros
   *
   * @return capacidad del vaso
   */
  public int capacidad() {
    return capacidad_ml;
  }

  /**
   * Dice qué contenido tiene el vaso en mililitros
   *
   * @return contenido del vaso
   */
  public int contenido() {
    return contenido_ml;
  }

  /**
   * Devuelve un string que representa un vaso.
   *
   * @return string que repreenta un vaso y su estado
   */
  public String toString() {
    int nivel = 5 * contenido_ml / capacidad_ml;
    String resultado =
      String.format("|      | %dml\n", capacidad_ml);
    for (int i = 4; nivel < i; i--)
      resultado += "|      |\n";
    if (contenido_ml > 0)
      resultado += String.format("|------| %dml\n", contenido_ml);
    else
      resultado += String.format("|      |\n");
      
    for (int i = nivel - 1; i >0; i--)
      resultado += "|      |\n";
    resultado += "|______| 0ml\n";
    return resultado;
  }
}
