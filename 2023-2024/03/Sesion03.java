public class Sesion03 {
  public static void main(String[] args) {
    Cancion c1;

    // Crear la canción de Quevedo
    c1 = new Cancion("Columbia", "Quevedo", 3*60 + 7);

    // Imprimir la canción c1
    System.out.println(c1);
  }
}
