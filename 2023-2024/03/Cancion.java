public class Cancion {
  String titulo;
  String interprete;
  int duracionSeg;
  boolean explicita;
  String album;

  Cancion(String t, String i, int dS) {
    titulo = t;
    interprete = i;
    duracionSeg = dS;
  }

  String minutos() {
    return duracionSeg / 60 + "mins";
  }

  String duracion() {
    int segs = duracionSeg % 60;
    // if (segs < 10)
    //   return "" + duracionSeg / 60 + ":0" + segs;
    // else
    //   return "" + duracionSeg / 60 + ":" + segs;
    return "" + duracionSeg / 60 + (segs < 10 ? ":0" : ":") + segs;
  }

  public String toString() {
    return
      "| " + titulo
      + " | " + interprete
      + " | " + this.duracion() + " |";
  }
}
