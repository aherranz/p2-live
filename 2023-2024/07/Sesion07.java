public class Sesion07 {
  public static void main(String[] args) {
    // Error de compilación:
    // Sesion07.java:3: error: non-static method absolutoV1(double) cannot be referenced from a static context
    // System.out.println(MisMates.absolutoV1(-3.0));
    MisMates m = new MisMates();
    // No tiene sentido invocar una función de R -> R usando un objeto m!!!
    System.out.println(m.absolutoV1(-3.0));
    System.out.println(MisMates.absolutoV2(-3.0));
  }
}
