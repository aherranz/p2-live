class Intercambiar {
  public static void intercambiar(int x, int y) {
    int aux = x;
    x = y;
    y = aux;
  }
  
  public static void main(String[] args) {
    int a, b;
    a = 27;
    b = 42;
    intercambiar(a,b);
    System.out.println(a + " - " + b);
  }
}
