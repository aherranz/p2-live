public class MisMates {
  public double absolutoV1(double x) {
    return x < 0.0 ? -x : x;
  }
  public static double absolutoV2(double x) {
    return x < 0.0 ? -x : x;
  }
}
