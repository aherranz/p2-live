class Sesion05 {
  public static void main(String[] args) {
    Punto2D a = new Punto2D();
    Punto2D b = new Punto2D(1.0, 2.0);
    System.out.format("Punto A: (%.2f, %.2f)\n", a.x, a.y);
    System.out.format("Punto B: (%.2f, %.2f)\n", b.x, b.y);
    System.out.println("Distancia a-b: " + a.distancia(b));
  }
}
