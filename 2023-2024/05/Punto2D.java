class Punto2D {
  private double x;
  private double y;

  Punto2D() {
    x = 0.0; y = 0.0;
  }

  Punto2D(double x, double y) {
    this.x = x; this.y = y;
  }

  double distancia(Punto2D p) {    
    return Math.sqrt((p.x - this.x)*(p.x - this.x)
                     + (p.y - this.y)*(p.y - this.y));
  }

  public double coordX() {
    return this.x;
  }
  
}
