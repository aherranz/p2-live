/*
 * Implementación clásica de una estructura de datos de "cadena
 * simplemente enlazada".
 */
class Nodoint {
  int dat;
  Nodoint sig;
  Nodoint(int d, Nodoint s) {
    dat = d;
    sig = s;
  }
}

/*
 * Esta clase es un programa principal que tiene una variable global
 * 'cadena' que es una cadena enlazada de enteros.
 *
 * Las operaciones, todas static, modifican la cadena enlazada
 * 'cadena' para ilustrar operaciones de cadenas enlazadas.
 *
 */
public class OperaConNodo {
  static Nodoint cadena;

  /*
   * (Re)inicializa cadena a una cadena que representa la colección
   * de elementos [42, 27, 37]
   */
  static void iniciar_42_27_37() {
    cadena = new Nodoint(42,new Nodoint(27,new Nodoint(37,null)));
  }

  /*
   * (Re)inicializa cadena a una cadena vacía ([])
   */
  static void iniciar_vacia() {
    cadena = null;
  }
  
  /*
   * Imprime en la salida estándar, de forma bonita, la cadena
   * enlazada 'cadena'
   */
  static void print() {
    if (cadena == null)
      System.out.println("[]");
    else {
      System.out.print("[" + cadena.dat);
      Nodoint aux = cadena.sig;
      while (aux != null) {
        System.out.print(", " + aux.dat);
        aux = aux.sig;
      }
      System.out.println("]");
    }
  }
 
  /*
   * Inserta un nodo con dato x en el final de la cadena enlazada
   * 'cadena'.
   */
  static void insertarFinal(int x) {
    if (cadena == null) {
      cadena = new Nodoint(x, null);
    }
    else {
      Nodoint aux = cadena;
      while (aux.sig != null)
        aux = aux.sig;
      aux.sig = new Nodoint(x, null);
    }
  }
 
  public static void main(String[] args) {
    iniciar_42_27_37();

    print();

    insertarFinal(100);

    print();

    iniciar_vacia();
    insertarFinal(100);    

    print();
  }
}



