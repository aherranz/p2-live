public class ProcesarOrdenes {
  public static void main(String[] args) {
    Cancion[] playlist = new Cancion[10];
    int siguiente = 0;
    while (StdIn.hasNextLine()
           // Cuidado no salirse de la playlist
           && siguiente < playlist.length) {
      String l = StdIn.readLine();
      String t, a, v;      
      switch (l) {
      case "a":
        System.err.println("He encontrado una orden de AÑADIR");
        t = StdIn.readLine();
        a = StdIn.readLine();
        v = StdIn.readLine();
        // Añadir la canción t, a, v a la lista playlist
        Cancion c = new Cancion(t, a, Integer.parseInt(v));
        playlist[siguiente] = c;
        siguiente++;
        break;
      case "r":
        System.err.println("He encontrado una orden de BORRAR");
        t = StdIn.readLine();
        // Borrar la canción t de la lista playlist
        break;
      default:
        System.err.println("ERROR EN ORDEN");
        break;
      }
    }
    for (int i = 0; i < siguiente; i++) {
      System.out.println(playlist[i]);
    }
  }
}
