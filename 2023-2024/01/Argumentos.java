public class Argumentos {
  public static void main(Strings[] args) {
    // args es un array de String (eso es lo que significa "String[]")
    // cada elemento son las palabras que se ponen como argumentos en
    // la línea de comandos
    // Prueba a ejecutar java Argumentos 1 DOS III 100
    for (int i = 0; i < args.length; i++) {
      System.out.println("El argumento " + i + " es " + args[i])
    }
  }
}
