public class PruebaHerencia {
  public static void main(String[] args) {
    Mamifero m = new Mamifero();
    m.comunicar();

    Perro p = new Perro("Toby");
    p.comunicar();
    p.ladrar();

    Mamifero m2 = new Perro("Noa");
    m2.comunicar();
    // Error de compilación porque Java es muy "restrictivo" y no
    // quiere que te equivoques
    m2.ladrar();
    // Se puede evitar haciendo un casting pero si m2 no es un perro
    // el código se rompería con una excepción
    ((Perro)m2).ladrar();

    // Ejemplos de uso de variables de supertipo
    // IList<String> l = new LinkedList<String>();
    // Mamifero m3 = leerMamiferoDeFichero();

    // PROHIBIDO: no se sabe si un mamífero cualquier es un Perro.
    // Perro p2 = new Mamifero();
    // p2.ladrar();




    
    
  }
}
