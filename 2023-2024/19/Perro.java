public class Perro extends Mamifero {
  public Perro(String n) {
    nombre = n;
  }

  public void ladrar() {
    System.out.format("%s dice GUAU!\n", nombre);
  }
  
}
