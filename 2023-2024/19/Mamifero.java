public class Mamifero {
  String nombre;

  public Mamifero() {
    nombre = "sin nombre";
  }

  public void comunicar() {
    System.out.format("Hola soy un mamifero %s\n", nombre);
  }
}
