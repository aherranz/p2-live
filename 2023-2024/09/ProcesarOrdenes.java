public class ProcesarOrdenes {
  public static void main(String[] args) {
    Playlist pl = new Playlist(10);

    // Variables para hacer recorridos
    int i;
    while (StdIn.hasNextLine()
           // Cuidado no salirse de la playlist
           && !pl.estaLlena()) {
      String l = StdIn.readLine();
      String t, a, v;      
      switch (l) {
      case "a":
        System.err.println("He encontrado una orden de AÑADIR");
        t = StdIn.readLine();
        a = StdIn.readLine();
        v = StdIn.readLine();
        // Añadir la canción t, a, v a la lista playlist
        Cancion c = new Cancion(t, a, Integer.parseInt(v));
        pl.insertar(c);
        break;
      case "r":
        System.err.println("He encontrado una orden de BORRAR");
        t = StdIn.readLine();
        // Borrar la canción t de la lista playlist
        pl.borrar(t);
        break;
      default:
        System.err.println("ERROR EN ORDEN");
        break;
      }
    }
    System.out.println(pl);
  }
}
