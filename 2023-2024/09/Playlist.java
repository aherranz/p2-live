public class Playlist {
  private Cancion[] canciones;
  private int siguiente;

  public Playlist(int capacidad) {
    canciones = new Cancion[capacidad];
    siguiente = 0;
  }

  public boolean estaLlena() {
    return siguiente == canciones.length;
  }
  
  public void insertar(Cancion c) {
    canciones[siguiente] = c;
    siguiente++;
  }

  public void borrar(String t) {
    int i = 0;
    while (i < siguiente) {
      if (t.equals(canciones[i].titulo())) {
        siguiente--;
        canciones[i] = canciones[siguiente];
        canciones[siguiente] = null;
        // TODO (AH): esto está ¡¡¡PROHIBIDO!!!, habrá que
        // resolverlo
        break;
      }
      i++;
    }
  }

  public String toString() {
    String resultado = "";
    for (int i = 0; i < siguiente; i++)
      resultado += canciones[i] + "\n";
    return resultado;
  }
}
