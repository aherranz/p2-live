public class PilaDouble {
  private double[] datos = new double[1000];
  private int cima = -1;

  public PilaDouble() {
  }
  
  public double cima() {
    return datos[cima];
  }

  public void desapilar() {
    cima--;
  }

  public void apilar(double d) {
    cima++;
    datos[cima] = d;
  }

  public boolean esVacia() {
    return cima == -1;
  }

  public boolean esLlena() {
    return cima == datos.length - 1;
  }
}
