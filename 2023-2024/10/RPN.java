public class RPN {
  // PRECONDICIÓN: operador tiene que ser "+", "-", "*", o "/".
  private static double operar(String operador, double op1, double op2) {
    switch (operador) {
    case "+":
      return op1 + op2;
    case "-":
      return op1 - op2;
    case "*":
      return op1 * op2;
    case "/":
      return op1 / op2;
    }
    return 0.0;
  }
  
  public static void main(String[] args) {
    double operando;
    double op1;
    double op2;
    double resultado;

    PilaDouble pila = new PilaDouble();

    // Mientras haya datos
    while (!StdIn.isEmpty()) {
    //   Leer el token
         String token = StdIn.readString();
         switch (token) {
    //   Si el token es un operador
         case "+":
         case "-":
         case "*":
         case "/":
    //     Sacar un operando de la pila
           op2 = pila.cima();
           pila.desapilar();
    //     Sacar otro operando de la pila
           op1 = pila.cima();
           pila.desapilar();
    //     Hacer el cálculo con el operador
           resultado = operar(token, op1, op2);
           
    //     Guardar el cálculo en la pila
           pila.apilar(resultado);
           break;
         case "=":
    //     Sacar un operando de la pila           
           resultado = pila.cima();
           pila.desapilar();
           System.out.println(resultado);
           break;
         default:
    //   Si el token es un operando
    //     Meter el token en la pila
           operando = Double.parseDouble(token);
           pila.apilar(operando);
           break;
         }
    }
  }
}
